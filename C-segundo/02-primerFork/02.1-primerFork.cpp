#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
	
	
int
main (int argc, char *argv[])
{

	for (int i = 0; i < 3; i++)
		fork();

	printf ("El pid ahora mismo es: %i\n", getpid());

    return EXIT_SUCCESS;
}
