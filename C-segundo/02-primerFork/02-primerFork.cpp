#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
	
	
int
main (int argc, char *argv[])
{

	printf ("Este programa tiene el proceso: %i\n\n", getpid());

	printf ("El identificador del proceso 0 es: %i, su pare es: %i \n", getpid(), getppid());
	
	fork(); // a partir de aqui se parte el proceso en 2, la primera parte se ejecuta con el pid del primer printf, mientras que la segunda genera uno nuevo
		// si al iniciar el programa se genera el pid 01, una vez se ejecute el fork, los printf que se realicen tendran, por poner un ejemplo, 02.
	
	printf ("\n\n");

	printf ("El identificador del proceso 1 es: %i, su padre es: %i \n", getpid(), getppid());
	printf ("El identificador del proceso 2 es: %i, su padre es: %i \n", getpid(), getppid());
	printf ("El identificador del proceso 3 es: %i, su padre es: %i \n", getpid(), getppid());

    return EXIT_SUCCESS;
}
