#include <stdio.h>
#include <unistd.h>
#define _POSIX_SOURCE
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>

void hijo (void) {
  int i=1;

  do {
      printf ("%c", 'H');
      if ((i%60)==0) printf ("\n");
  } while (i++ < 1000000);
  printf ("Fin del proceso hijo\n");
  _exit(3);
}

int main(void) {
	  int pid, resultado, estado;
  char ch;

  pid = fork();
  if (pid==0) {
      hijo();
  } else {
      scanf("%c", &ch);
      resultado = kill (pid, SIGKILL);
      printf ("Enviado SIGKILL al hijo con resultado %d\n", resultado);
      resultado = wait(&estado);
      if (!(WIFEXITED(estado) > 0))
        printf ("Abortado %d con senial %d\n", resultado, WTERMSIG (estado));
      else
        printf ("Terminado hijo %d con estado %d\n", resultado, WEXITSTATUS (estado));

      return EXIT_SUCCESS;
  }
}

