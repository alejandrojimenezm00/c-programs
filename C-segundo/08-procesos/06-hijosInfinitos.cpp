#include <stdio.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>
#include <sys/resource.h>
#include <stdlib.h>

struct sigaction trataSenial;


void handle_sigint (int sig)
{
	printf ("Hijo: muelto\n");
}


int main(void) 
{
	int status;
	char espera;
	pid_t pid;

	signal (SIGINT, handle_sigint);

	while (1)
	{
		pid = fork();

		if (pid < 0)
			printf ("Error al crear al hijo\n");
		if (pid == 0)
		{
			pause();
		}
		else
		{
			sleep(1);
			printf ("Padre: otro hijo\n");
		}

	}

	return EXIT_SUCCESS;
}
