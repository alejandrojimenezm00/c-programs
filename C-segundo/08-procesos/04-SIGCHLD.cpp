#include <stdio.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>
#include <sys/resource.h>
#include <stdlib.h>

struct sigaction trataSenial;


void handle_sigusr1 (int sig)
{
	printf ("Señal de mi hijo recogida\n");
}

void handle_sigchld (int sig)
{
	printf ("Mi hijo ha terminado su ejecucion\n");
}

int main(void) 
{
	int padre = getpid();
	int childstatus;
	pid_t pid;

	signal (SIGUSR1, handle_sigusr1);
	signal (SIGCHLD, handle_sigchld);

	pid = fork();

	if (pid < 0)
		printf ("Error al crear el proceso hijo\n");

	if (pid == 0)
	{
                trataSenial.sa_handler = handle_sigusr1;

		printf ("\nEnviado señal\n");
		sleep (2);
		kill (padre, SIGUSR1);
		sleep (3);
		exit(0);
	}
	else
	{
		wait(&childstatus);
	}

	return EXIT_SUCCESS;
}
