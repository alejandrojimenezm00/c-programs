#include <stdio.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>
#include <sys/resource.h>
#include <stdlib.h>

struct sigaction trataSenial;


void handle_sigusr1 (int sig)
{
	printf ("Esta es la señal 1\n");
	kill (getpid(), SIGUSR2);
}

void handle_sigusr2 (int sig)
{
	printf ("Esta es la señal 2\n");
}


int main(void) 
{

	signal (SIGUSR1, handle_sigusr1);
	signal (SIGUSR2, handle_sigusr2);

	kill (getpid(), SIGUSR1);

}
