#include <stdio.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>
#include <sys/resource.h>
#include <stdlib.h>


struct sigaction trataSenial;

void handle_signal (int sig)
{
	printf ("SEÑAL RECIBIDA\n");
}



int
main (int argc, char *argv[])
{

	pid_t pid;

	pid = fork();

	if (pid < 0)
		printf ("Error al crear el proceso\n");

	if (pid != 0)
	{
		sleep (10);
		kill (pid, SIGUSR1);
		exit(0);
	} else
	{
		trataSenial.sa_handler = handle_signal;
		sigaction(SIGUSR1, &trataSenial, NULL);
		pause();
	}


	return EXIT_SUCCESS;
}
