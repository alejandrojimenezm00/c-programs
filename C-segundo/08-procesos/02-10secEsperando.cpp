#include <stdio.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>
#include <sys/resource.h>
#include <stdlib.h>

struct sigaction trataSenial;


void handle_alarm (int sig)
{
	printf ("no me has mandado nada\n");
	exit (0);
}


int main(void) 
{
	char dato = 0;

	signal (SIGALRM, handle_alarm);

	alarm (10);
	
	while (dato == 0)
	{
		scanf (" %c", &dato);
	}

	printf ("El dato que me has dado es: %c\n", dato);

}
