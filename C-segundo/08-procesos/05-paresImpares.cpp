#include <stdio.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>
#include <sys/resource.h>
#include <stdlib.h>

struct sigaction trataSenial;

int contador = 0;

void handle_sigusr1 (int sig)
{
	printf ("Padre: %i\n", contador++);
}


int main(void) 
{
	int padre = getpid();
	pid_t pid;

	signal (SIGUSR1, handle_sigusr1);

	do
	{
		if (pid < 0)
			printf ("Error al crear el proceso hijo\n");

		if (pid == 0)
		{
			if (contador % 2 != 0)
				printf ("Hijo: %i\n", contador++);
			kill (padre, SIGUSR1);
			sleep(1);
		}
	} while(contador <= 10);

	return EXIT_SUCCESS;
}
