#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/resource.h>


int
main (int argc, char *argv[])
{
	printf ("El proceso actual tiene prioridad: %i\n", getpriority(PRIO_PROCESS, getpid()));

    return EXIT_SUCCESS;
}
