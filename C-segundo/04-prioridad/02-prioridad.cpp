#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/resource.h>
#include <sys/time.h>


int
main (int argc, char *argv[])
{
	printf ("El proceso actual tiene prioridad: %i\n", getpriority(PRIO_PROCESS, getpid()));
	printf ("Ahora le cambiamos la prioridad\n");

	setpriority(PRIO_PROCESS, getpid(), 10);
	printf ("El proceso actual tiene prioridad: %i\n", getpriority(PRIO_PROCESS, getpid()));

    return EXIT_SUCCESS;
}
