#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/resource.h>


int
main (int argc, char *argv[])
{
	int pid_padre = getpid();
	printf ("El proceso actual tiene prioridad: %i, y su id es: %i\n", getpriority(PRIO_PROCESS, pid_padre), getpid());

	if (fork() != 0)
	{
		setpriority (PRIO_PROCESS, pid_padre, 15);
		exit (0);
	}	

	sleep (100);
	



    return EXIT_SUCCESS;
}
