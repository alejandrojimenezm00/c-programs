#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/resource.h>


int
main (int argc, char *argv[])
{
	int pid_padre = getpid();
	printf ("El proceso actual tiene prioridad: %i, y su id es: %i\n", getpriority(PRIO_PROCESS, pid_padre), getpid());

	if (fork() != 0)
	{	
		setpriority(PRIO_PROCESS, getpid(), 10);
		printf ("Me he quedado huerfanito, mi padre ahora es: %i\n", getppid());
	}
	



    return EXIT_SUCCESS;
}
