#include <stdio.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>
#include <sys/resource.h>
#include <stdlib.h>


void handle_sigint (int sig)
{
	printf ("\nmi padre me evia una señal\n");
}

void handle_SIGQUIT (int sig)
{
	printf ("\nmi padre me ha alto asesinado\n");
}

int main()
{
	int pid;

	if ((pid = fork()) < 0)
	{
		printf ("Error al crear el proceso hijo\n");
		exit(1);
	}

	if (pid == 0) 
	{
		signal(SIGINT, handle_sigint);
		signal(SIGINT, handle_sigint);
		signal(SIGINT, handle_sigint);
		signal(SIGINT, handle_sigint);
		signal(SIGQUIT, handle_SIGQUIT);

		while (1);
	}
	else
	{
		printf ("Primera señal\n");
		kill (pid, SIGINT);

		printf ("Segunda señal\n");
		kill (pid, SIGINT);


		printf ("Tercera señal\n");
		kill (pid, SIGINT);


		printf ("Cuarta señal\n");
		kill (pid, SIGINT);


		printf ("Quinta señal\n");
		kill (pid, SIGQUIT);

	}
}


