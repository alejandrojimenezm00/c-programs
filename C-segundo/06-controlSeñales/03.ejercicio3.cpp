#include <stdio.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>
#include <sys/resource.h>
#include <stdlib.h>


void handle_sigint (int sig)
{
	printf ("mi PID es: %i\n", getpid());
}
int main()
{
	signal (SIGUSR1, handle_sigint);
	while (1)
	{
		raise(SIGUSR1);
		sleep(1);
	}
	return EXIT_SUCCESS;
}


