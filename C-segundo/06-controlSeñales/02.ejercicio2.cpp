#include <stdio.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>
#include <sys/resource.h>
#include <stdlib.h>

int i = 20;

void handle_sigint (int sig)
{
	i -= 1;
	printf ("El valor de I = %i\n", i);
}
void handle_sigquit (int sig)
{
	i += 3;
	printf ("El valor de I = %i\n", i);
}
void handle_sigtstp (int sig)
{
	i += 5;
	printf ("El valor de i es: %i\n", i);
}
int main()
{
	signal (SIGINT, handle_sigint);
	signal (SIGQUIT, handle_sigquit);
	signal (SIGTSTP, handle_sigtstp);
	while (1)
	{
		printf ("El contador ahora mismo vale: %i\n", i);
		sleep(1);
	}
	return EXIT_SUCCESS;
}


