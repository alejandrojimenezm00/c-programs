#include <stdio.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>
#include <sys/resource.h>
#include <stdlib.h>
#include <unistd.h>



void handle_sigint(int sig)
{
	printf ("he sido llamado tras una alarma\n");
}

void handle_alarm (int sig)
{
	raise (SIGINT);
	kill (getpid(), SIGINT);
}
int main()
{
	signal (SIGALRM, handle_alarm);
	signal (SIGINT, handle_sigint);

	alarm(5);
	while (1)
	{
		printf ("Esperando a SIGINT\n");
		sleep(1);
	}

	return EXIT_SUCCESS;
}


