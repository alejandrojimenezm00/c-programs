#include <stdio.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>
#include <sys/resource.h>
#include <stdlib.h>


int contador = 0;
void handle_sigint (int sig)
{
	printf ("Han pasado: %i segundos\n", contador);
}
int main()
{
	signal (SIGINT, handle_sigint);
	while (1)
	{
		contador++;
		sleep(1);
	}
	return EXIT_SUCCESS;
}


