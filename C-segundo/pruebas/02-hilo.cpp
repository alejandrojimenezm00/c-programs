#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>


void *funcion (void *dato)
{
	printf ("%i\n", dato+2);
}


int main()
{

	int dato = 2;
	pthread_t id_hilo;

	//id_hilo identificador del hilo
	//null atributos del hilo
	//funcio, funcion que realiza el hilo
	//dato, datos que le pasamos
	pthread_create(&id_hilo, NULL, funcion, (void *)dato);

	pthread_join(id_hilo, NULL);

	return EXIT_SUCCESS;
}
