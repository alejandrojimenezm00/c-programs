#include <assert.h>
#include <stdio.h>
#include <unistd.h>

int tubo[2];

int main(void) {
	int idHijo, estado;

	pipe(tubo);
	if (fork() != 0) {
		/* El Padre ejecuta el comando ls */
		dup2 (tubo[1], 1);  /* Salida estandar => tubo[1] */
		close(tubo[0]);     /* Cerramos extremo lectura   */
		execl("/bin/ls", "ls", "-l", NULL);
	} else {
		/* El Hijo  ejecuta el comando wc */
		dup2 (tubo[0], 0);  /* Entrada estandar => tubo[0] */
		close(tubo[1]);     /* Cerramos extremo escritura */
		execl("/usr/bin/wc", NULL);
	} 

	return 0;
}

