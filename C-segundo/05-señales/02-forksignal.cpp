#include <stdio.h>
#include <wait.h>
#include <signal.h>
#include <unistd.h>
#include <sys/resource.h>
#include <stdlib.h>

int main()
{
	int stat;
	pid_t pid;

	if ((pid = fork()) == 0)
		while (1);
	else
	{
		kill (pid, SIGINT);
		wait (&stat);
		if (WIFSIGNALED (stat))
			psignal (WTERMSIG (stat), "El hijo tambien hace cosas");
	}
}
