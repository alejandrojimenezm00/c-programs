#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/resource.h>

void handle_sigint (int sig)
{
	printf ("Atrapando la señal\n", sig);
}
int main()
{
	signal (SIGINT, handle_sigint);
	while (1)
	{
		printf("Hellow world\n");
		sleep(1);
	}
	return EXIT_SUCCESS;
}

