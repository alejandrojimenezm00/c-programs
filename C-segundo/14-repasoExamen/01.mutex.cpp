#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <fcntl.h>
#include <errno.h>

static int contador = 10;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void *sumador (void *arg)							
{
	for (int i = 0; i < 100000; i++)
	{
		pthread_mutex_lock (&mutex);
		contador++;
		pthread_mutex_unlock (&mutex);
	}
}

void *restador (void *args)
{
	for (int i = 0; i < 100000; i++)
	{
                pthread_mutex_lock (&mutex);
                contador--;
                pthread_mutex_unlock (&mutex);

	}
}

int main (int argc, char *argv[])
{
	pthread_t hilo1, hilo2; 						
	
		

	pthread_create (&hilo1, NULL, sumador, NULL);				
	pthread_create (&hilo2, NULL, restador, NULL);			

	pthread_join (hilo1, NULL);
	pthread_join (hilo2, NULL);	

	printf ("contador vale: %d\n", contador); 


	return EXIT_SUCCESS;
}
