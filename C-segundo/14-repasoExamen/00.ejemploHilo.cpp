#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

int contador = 0;

void *sumador (void *arg)							// funcion que va a trabajar el hilo
{
	int valor = *((int*)arg);

	for (int i = 0; i <= valor; i++)
		contador += i;
}


int main (int argc, char *argv[])
{
	pthread_t hilo1; 							// declaracion del hilo
	
	int i;

	printf ("Dime un numero y te calculo su sumatorio: ");
	scanf  (" %i", &i);

	pthread_create (&hilo1, NULL, sumador, &i);				// funcion que iniciará el hilo, Sintaxis ->
										// hilo1: puntero al hilo declarado, NULL argumentos, sumador: funcion que trabajará el hilo, parametros (siempre poner &)
	pthread_join (hilo1, NULL);						// Funcion que duerme el main hasta que el hilo se acaba

	printf ("el sumador de hasta %i es: %i\n", i, contador);


	return EXIT_SUCCESS;
}
