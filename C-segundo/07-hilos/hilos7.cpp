/*------------------------------------------------------*/
/* unminuto.c: Segundero corriendo unos 60 segundos */
/*------------------------------------------------------*/
#include <stdio.h>
#define _POSIX_SOURCE
#include <signal.h>
#include <unistd.h>

struct sigaction trataSenial;
void tic (int i) {
}
main(void) {
	int segundos;
	trataSenial.sa_handler = tic;
	sigaction (SIGALRM, &trataSenial, NULL);
	for (segundos=1; segundos<61; segundos++) {
		alarm(1);
		pause();
		printf ("%2d\n", segundos);
	}
}

