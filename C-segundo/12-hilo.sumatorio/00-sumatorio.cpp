#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>


int sumatorio = 0;
int sumador = 0;

void *funcion (void *dato)
{
	sumatorio += sumador;
	sumador++;
}


int main()
{

	int dato = 2;
	pthread_t id_hilo;

	for (int i = 0; i <= 5; i++)
		pthread_create(&id_hilo, NULL, funcion, (void *)dato);

	pthread_join(id_hilo, NULL);

	printf ("%i\n", sumatorio);

	return EXIT_SUCCESS;
}

