#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 2 

int main( int argc, char **argv )
{
	pid_t pid;
	int a[2], b[2], readbytes;
	char buffer[SIZE];

	pipe( a );
	pipe( b );

	if ( (pid=fork()) == 0 )
	{ // hijo
		close( a[1] ); /* cerramos el lado de escritura del pipe */
		close( b[0] ); /* cerramos el lado de lectura del pipe */

		while( (readbytes=read( a[0], buffer, SIZE ) ) > 0)
			write( 1, buffer, readbytes );
		close( a[0] );

	}
	else
	{ // padre
		close( a[0] ); /* cerramos el lado de lectura del pipe */
		close( b[1] ); /* cerramos el lado de escritura del pipe */
	
		for (int i = 0; i < 10; i++)
		{
			if (i % 2 == 0)
			strcpy( buffer, "a" );
			else
			strcpy( buffer, "b" );
			
			write( a[1], buffer, SIZE);
		}
		
		close( a[1]);


	}
	printf ("\n");
	waitpid( pid, NULL, 0 );
	exit( 0 );
}
