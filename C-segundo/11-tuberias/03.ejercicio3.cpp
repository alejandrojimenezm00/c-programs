#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 28 

int main( int argc, char **argv )
{
	pid_t pid;
	int a[2], b[2], readbytes;
	char buffer[SIZE];

	pipe( a );
	pipe( b );

	if ( (pid=fork()) == 0 )
	{ // hijo
		close( a[1] ); /* cerramos el lado de escritura del pipe */
		close( b[0] ); /* cerramos el lado de lectura del pipe */

		while( (readbytes=read( a[0], buffer, SIZE ) ) > 0)
		{
			if (strcmp(buffer, "5") && strcmp(buffer, "6"))
				write (1, "Estas aprobado por los pelos", SIZE);
			else if (strcmp(buffer, "6"))
				write (1, "Bien hecho, toma la paga", SIZE);
			else
				write (1, "debes estudiar mas", SIZE);

		}
		close( a[0] );

	}
	else
	{ // padre
		close( a[0] ); /* cerramos el lado de lectura del pipe */
		close( b[1] ); /* cerramos el lado de escritura del pipe */

		strcpy( buffer, "8" );
		write( a[1], buffer, SIZE);

		close( a[1]);


	}
	printf ("\n");
	waitpid( pid, NULL, 0 );
	exit( 0 );
}
