#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 256

	int
main (int argc, char *argv[])
{
	char cadena[SIZE];
	int num, readbytes;
	pid_t pidHijo, pidNieto;
	int padreHijo[2], hijoNieto[2];

	pipe(padreHijo);
	pipe(hijoNieto);

	pidHijo = fork();

	if (pidHijo == 0)
	{
		pidNieto = fork();
		if (pidNieto == 0)
		{
			close (padreHijo[0]);
			close (padreHijo[1]);
			close (hijoNieto[1]);

			while ((readbytes = read (hijoNieto[0], cadena, SIZE)) > 0)
			{
				printf ("Los datos que sacamos del nieto son: %s\n", cadena);



			}
			printf ("EL resultado traducido es: %s\n", cadena);
			close (hijoNieto[0]);

		} 
		else
		{
			close (padreHijo[1]);
			close (hijoNieto[0]);

			while ((readbytes = read(padreHijo[0], cadena, SIZE))>0)
			{
				printf ("los datos que sacamos de la tuberia padreHijo son %s\n", cadena);


				num = atoi (cadena);
				switch (num)
				{
					case 0: strcpy(cadena, "A");
						break;
					case 1: strcpy(cadena, "B");
						break;
					case 2: strcpy(cadena, "C");
						break;
				}

			}

			close (padreHijo[0]);

			write(hijoNieto[1], cadena, strlen(cadena));
			close(hijoNieto[1]);
			waitpid(pidNieto, NULL, 0);
		}
	}
	else
	{
		close (padreHijo[0]);
		close (hijoNieto[0]);
		close (hijoNieto[1]);

		printf ("escribe: ");
		scanf ("%i", &num);
		sprintf (cadena, "%i", num);
		write (padreHijo[1], cadena, SIZE);
		close (padreHijo[1]);

		waitpid (pidHijo, NULL, 0);
	}


	return EXIT_SUCCESS;
}

