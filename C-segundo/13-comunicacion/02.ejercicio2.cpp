#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 20

int
main (int argc, char *argv[])
{
	char cadena[SIZE];


	pid_t pid;

	int a[2], b[2], readbytes;

	pipe (a);
	pipe (b);

	if ((pid = fork()) == 0)
	{
		close (a[1]);
		close (b[0]);

		while ((readbytes = read (a[0], cadena, SIZE)) > 0)	
			for (int i = strlen(cadena); i > 0; i--)
				printf ("%c", cadena[i]);
	
		close (a[0]);
		
	}
	else
	{
		close (a[0]);
		close (b[0]);

		strcpy (cadena, "buenos dias");
		write (a[1], cadena, SIZE);

		close (a[1]);
	}



    return EXIT_SUCCESS;
}
