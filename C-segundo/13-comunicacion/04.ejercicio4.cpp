#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>	
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 10

	int
main (int argc, char *argv[])
{
	char cadena[SIZE];
	char num1[3], num2[5];
	int num, readbytes;
	pid_t pidHijo, pidNieto;
	int padreHijo[2], padreHijo2[2];
	int padreHijoIda[2], padreHijoVuelta[2];

	pipe(padreHijo);
	pipe(padreHijo2);
	pipe(padreHijoVuelta);
	pipe(padreHijoIda);

	pidHijo = fork();

	if (pidHijo == 0)
	{
		close (padreHijo[1]);
		close (padreHijo[0]);
		close (padreHijo2[1]);
		close (padreHijoVuelta[0]);
		close (padreHijoVuelta[1]);

		while ((readbytes = read (padreHijo2[0], num2, strlen(num2)) >0))
			//for (int i = 0; i < strlen(num2); i++)
			printf ("%s", num2);
		write (padreHijoIda[1], num2, strlen(num1));
		close (padreHijoIda[1]);
		close (padreHijo2[0]);

	}
	else
	{
		if ((pidNieto = fork()) == 0)
		{
			close (padreHijo2[1]);
			close (padreHijo2[0]);
			close (padreHijo[1]);
			close (padreHijoIda[0]);
			close (padreHijoIda[1]);

			int numReps;

			while ((readbytes = read (padreHijo[0], num1, strlen(num1)) >0))
				numReps = atoi(num1);
			
			char nums1[numReps];

			for (int i = 0; i < strlen(numReps); i++)
				nums1[i] = i + '0';

			write (padreHijoVuelta[1], nums1, strlen(num1));
			close (padreHijoVuelta[1]);
			close (padreHijo[0]);	

		}
		else
		{ //cierrar las tuberias
			close (padreHijo[0]);
			close (padreHijo2[0]);
			close (padreHijoIda[1]);
			close (padreHijoVuelta[1]);

			strcpy(num1,"2");
			strcpy(num2,"3");

			write (padreHijo[1], num1, strlen(num1));
			write (padreHijo2[1], num2, strlen(num2));

			close(padreHijo[1]);
			close(padreHijo2[1]);


			while ((readbytes = read (padreHijoVuelta[0], num1, strlen(num1)) > 0))
				//for (int i = 0; i < strlen(num1); i++)
				printf ("%s", num1);

			while ((readbytes = read (padreHijoIda[0], num2, strlen(num2)) > 0))
				//for (int i = 0; i < strlen(num2); i++)
				printf ("%s", num2);

		}



	}


	return EXIT_SUCCESS;
}

