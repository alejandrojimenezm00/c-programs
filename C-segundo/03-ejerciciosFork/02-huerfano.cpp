#include <stdio.h>
#include <unistd.h>
int main(void)
{

	pid_t pidC;

	int padre = getpid();

	pidC = fork();

	if (pidC > 0)
		printf ("Soy el padre y este es mi PID: %i\n", getpid());
	
	else if (pidC == 0)
	{
		sleep (5);
		printf ("Soy el hijo y este es mi PID: %i, mi padre es: %i\n", getpid(), getppid());
		
	}
	else 
		printf ("error, no se esta ejecutando nada\n");


}
