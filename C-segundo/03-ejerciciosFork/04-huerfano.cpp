#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
int main(void)
{

	pid_t pidC;

	pidC = fork();

	while (1)
	{
		sleep(1);
		if (pidC > 0)
			printf ("Soy el padre y este es mi PID: %i\n", getpid());
		else if (pidC == 0)
			printf ("Soy el hijo y este es mi PID: %i\n", getpid());
		else 
			printf ("error, no se esta ejecutando nada\n");

		printf ("El estado actual de los procesos es: \n\n");
		system ("ps -lf");
	}

}
