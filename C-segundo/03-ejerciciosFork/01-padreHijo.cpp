#include <stdio.h>
#include <unistd.h>
int main(void)
{

    printf ("este es el PID del padre: %i\n", getpid());
    fork();
    printf ("este es el PID del hijo: %i, cuyo padre es: %i\n", getpid(), getppid());

}
