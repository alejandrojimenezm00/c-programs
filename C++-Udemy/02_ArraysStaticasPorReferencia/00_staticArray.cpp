#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <algorithm>


using namespace std;


//
//Funcion para imprimir arrays por pantalla
//
//El primer parametro es la posicion de memoria del array, el segundo es el tamaño del array
//
void Print (int *ptr, int size)
{
	for (int i = 0; i < size; i++)
	{
		std::cout << ptr[i] << ' ';
	}
}


//
//Uso de template para simplificar el código
//
template <typename T, int size>
void Print (T(&ref)[size])
{
        for (int i = 0; i < size; i++)
        {
                std::cout << ref[i] << ' ';
        }

}

template <typename T, int size>
T * Inicio (T(&ref)[size])
{
	return ref;
}

template <typename T, int size>
T * Final (T(&ref)[size])
{
        return (ref + size);
}


int
main (int argc, char *argv[])
{

	//
	//formas de inicializar arrays
	//
	
	int arr1[5];
	int arr2[5]{};
	int arr3[5]{1,2,3,4,5};

	
	for (int i = 0; i < 5; i++)
		printf ("%i", arr3[i]);
	printf ("\n\n\n");

	//
	//forma de "copiar" un array 
	//
	//tipo de dato *puntero (puntero + variable) = array (primera posicion del array)
	//
	//Con esta declaración lo que se consigue realmente es acceder literalmente a las zonas de memoria del array y poder modificarlas
	//
	int *p = arr3;

	//
	//Acceso a las posiciones del array, primera posicion
	//
	*(p) = 100;
	printf ("%i", p[0]);
        printf ("\n\n\n");


	//
	//Segunda posicion
	//
        *(p + 3) = 300;
        printf ("%i", p[3]);
        printf ("\n\n\n");


	//
	//Arrays por referencia
	//
	int (&ref)[5] = arr3;

	
	//
	//Como parametro le pasamos el "array" siendo realmente su primera posicion, y un tamaño para operar
	//
	//Print(arr3, sizeof(arr3) / sizeof (int));
	
	//
	//Ordenar un array
	//
	int *inicial = Inicio(arr3);
	int *fin = Final(arr3);
	std::sort(inicial, fin);

	Print(arr3);
	
	printf ("\n");


    return EXIT_SUCCESS;
}
