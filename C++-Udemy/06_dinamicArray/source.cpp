#include "GArray.h"
#include <iostream>

void Print(const Basic::GArray<int> &arr, const char *message)
{
	std::cout << message << std::endl;
	for (int i = 0; i < arr.GetSize(); i++)
	{
	       std::cout << arr[i] << ' ';
	}
	std::cout << std::endl;

}

int main()
{
	using namespace Basic;
	GArray <int> arr {1,2,3};

	Print (arr, "initial Array");
}
