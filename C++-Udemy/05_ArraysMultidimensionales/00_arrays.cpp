#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>

void 
Print (int(*p)[3], int rows)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			std::cout << p[i][j] << ' ';
		}
		std::cout << std::endl;
	}

}



template <typename T, int rows, int columns>
void Print (T(&ref)[rows][columns])
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			std::cout << ref[i][j] << ' ';
		}
		std::cout << std::endl;
	}

}
int
main (int argc, char *argv[])
{

	srand (time(NULL));

	int arr[2][3] = {
		{1,2,3}, 
		{4,5,6}
	};

	int (*p)[3] = arr;
	p[0][0] = 100;

	Print (arr);

	printf ("\n\n\n");

	int **pp = new int *[2];
	pp[0] = new int[3];
	pp[1] = new int[3];

	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			pp[i][j] = rand() % 10;
		}
	}

	for (int i = 0; i < 2; i++)
        {
                for (int j = 0; j < 3; j++)
                {
			std::cout << pp[i][j] << ' ';
                }
		printf ("\n");
        }
	delete[] pp[0];
	delete[] pp[1];

	delete[]pp;


	return EXIT_SUCCESS;
}
