#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{

	//
	//formas de inicializar arrays
	//
	
	int arr1[5];
	int arr2[5]{};
	int arr3[5]{1,2,3,4,5};

	
	for (int i = 0; i < 5; i++)
		printf ("%i", arr3[i]);
	printf ("\n\n\n");

	//
	//forma de "copiar" un array 
	//
	//tipo de dato *puntero (puntero + variable) = array (primera posicion del array)
	//
	int *p = arr3;

	//
	//Acceso a las posiciones del array, primera posicion
	//
	*(p) = 100;
	printf ("%i", p[0]);
        printf ("\n\n\n");


	//
	//Segunda posicion
	//
        *(p + 3) = 300;
        printf ("%i", p[3]);
        printf ("\n\n\n");





    return EXIT_SUCCESS;
}
