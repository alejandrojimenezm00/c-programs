#include <stdio.h>
#include <stdlib.h>

void imprime (int num) {
    static int cont = 0;
    printf ("%i * %i = %i\n", num, cont, num * cont++);
}

int
main (int argc, char *argv[])
{
    int num;
    printf ("Numero: ");
    scanf (" %i", &num);

    for (int i = 0; i < 10; i++)
        imprime (num);

    return EXIT_SUCCESS;
}
