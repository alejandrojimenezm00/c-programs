#include <stdio.h>
#include <stdlib.h>

void suma (int num1, int num2) {
    printf ("El numero es: %i\n", num1 + num2);
}

int
main (int argc, char *argv[])
{
    void (*pf) (int, int) = {suma};

    pf(2, 3);

    return EXIT_SUCCESS;
}
