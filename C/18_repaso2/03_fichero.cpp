#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 0x100

int
main (int argc, char *argv[])
{
    FILE *f;
    char palabra[MAX];

    if (! (f = fopen ("file.txt", "w")))
    {
        printf ("El fichero no se ha podido abrir\n");
        return EXIT_FAILURE;
    }

    do
    {
        scanf (" %s", palabra);
        fprintf (f, "%s", palabra);
    } while (strcmp (palabra, "fin") != 0);
    

    fclose (f);

    return EXIT_SUCCESS;
}
