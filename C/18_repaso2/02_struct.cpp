#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct examen {
    int nota;
    char *alumno;
};

void imprime (const struct examen *ejemplo) {
    printf ("El alumno %s ha sacado un %i\n", ejemplo -> alumno, ejemplo -> nota);
}

int
main (int argc, char *argv[])
{
    struct examen primer = {6, NULL};

    primer.alumno = (char *) malloc (sizeof (char));
    strcpy (primer.alumno, "Antonio");

    imprime (&primer);


    return EXIT_SUCCESS;
}
