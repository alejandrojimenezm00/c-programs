#include <stdio.h>
#include <stdlib.h>

void calculo (int num) {
    if (num > 0)
        calculo (num / 2);
    printf ("%i", num % 2);
}

int
main (int argc, char *argv[])
{
    int binario;
    printf ("Numero: ");
    scanf (" %i", &binario);

    printf ("\n");
    calculo (binario);
    printf ("\n");

    return EXIT_SUCCESS;
}
