#include <stdio.h>
#include <stdlib.h>

int suma (int num1, int num2) {
    return num1 + num2;
}

int
main (int argc, char *argv[])
{
    int (*pf) (int, int) = {suma};

    printf ("El numero es: %i\n",pf(2, 3));

    return EXIT_SUCCESS;
}
