#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>
int
main (int argc, char *argv[])
{
    char **coches = NULL;
    char **p;
    int sum = 0;
    int veces = 0;

    do {
        system ("clear");
        coches = (char **) realloc (coches, (sum + 1) * sizeof (char *));
        printf ("Coche: ");
        veces = scanf ("%m[a-z A-Z]", coches + sum++);
        __fpurge (stdin);
    } while (veces);

    system ("clear");
    p = coches;

    while (*p!=NULL)
    {
        printf ("%s\n", *p);
        p++;
    }

    for (char **i = coches; *i!=NULL; i++)
        free (*i);
    free (coches);

    return EXIT_SUCCESS;
}
