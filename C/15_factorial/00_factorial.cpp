#include <stdio.h>
#include <stdlib.h>


unsigned
factorial (unsigned num)
{
	return num != 0 ? num * factorial (num - 1) : 1;
}

int
main (int argc, char *argv[])
{
	unsigned num;

	printf ("Dime un numero para su factorial: ");
	scanf (" %u", &num);

	system("clear");
	printf ("\nEl factorial de %u es: %u\n\n", num, factorial(num));


    return EXIT_SUCCESS;
}
