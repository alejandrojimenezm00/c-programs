#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#define MENU printf("Bienvenido al menú, especifica el nùmero de la figura que quieras ver para decirte la página en la que se encuentra \n****************************\n* 1. Cuadrado              *\n* 2. Circulo               *\n* 3. Ovalo                 *\n* 4. Triangulo             *\n* 5. Octogono              *\n* **************************\n");


using namespace std;
int
main (int argc, char *argv[])
{
	enum Figuras {Cuadrado=10, Circulo, Ovalo, Triangulo, Octogono};
	int figura;

	MENU;

	printf("\n");
	scanf("%d", &figura);
	if (figura == 1)
	{
		cout << Cuadrado;
		printf("\n");
	}
	if (figura == 2)
	{
		cout << Circulo;
		printf("\n");
	}
	if (figura == 3)
	{
		cout << Ovalo;
		printf("\n");
	}
	if (figura == 4)
	{
		cout << Triangulo;
		printf("\n");
	}
	if (figura == 5)
	{
		cout << Octogono;
		printf("\n");
	}

	return EXIT_SUCCESS;
}
