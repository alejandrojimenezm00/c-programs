#include <stdio.h>
#include <stdlib.h>

#define MENU printf("******************\n* 1. Sumar       *\n* 2. Restar      *\n* 3. Multiplicar *\n* 4. Dividir     *\n* 5. Potencias   *\n******************\n        ");

#define NUM 4
#define ELVALOR(x)  printf("El valor de " #x " es: %i\n", x);
int
main (int argc, char *argv[])
{

    int a = NUM;
    MENU;
    ELVALOR(NUM);
    printf("\n"__FILE__);
    printf("%i\n", __LINE__);
    return EXIT_SUCCESS;
}
