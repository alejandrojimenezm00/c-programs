#include <stdio.h>
#include <stdlib.h>

/*
 * Se puede compilar y definir una constante
 *
 * g++ -DNDEBUG -o parabola parabola.cpp
 */

#define XMIN  -4
#define XMAX   4
#define STEP 0.1
#define N  (int) ((XMAX - XMIN) / STEP)

#ifndef NDEBUG
#define INFORMA(...) \
    fprintf (stderr, "[%i] ", __LINE__);    \
    fprintf (stderr, "INFO: " __VA_ARGS__); \
    fprintf (stderr, "\n");
#else
#define INFORMA(...)
#endif

int
main (int argc, char *argv[])
{
    double f[N];
    double deri[N];

#ifdef NDEBUG
    system("clear");
#endif

    INFORMA("Título");
    system("toilet -fpagga --gay PARABOLA");

    INFORMA("Calculando...");
    double x = XMIN;
    double y = XMAX;
    for (int i=0; i<N; i++, x+=STEP)
        f[i] = x * x + 3;

    for (int i=0; i<N; i++, x+=STEP)
        deri[i] = y / x - STEP;


    INFORMA("Imprimiendo...");
    x = XMIN;
    for (int i=0; i<N; i++, x+=STEP)
        printf ("%lf\t%lf\t%lf\n", x, f[i], deri[i]);


    return EXIT_SUCCESS;
}
