#include <stdio.h>
#include <stdlib.h>

//con las constantes simbolicas no se puede añadir comentarios despues de la constante con //:
#define EURO 166.386

//tambien se pueden definir macros
#define imprime       printf
#define PON_ENTERO(x) printf("%i\n", (x));
#define SUMA2(x)      x + 2
#define SUMA(x,y)     x + y
#define ESCRIBE(...)  printf (__VA_ARGS__)
int
main (int argc, char *argv[])
{

    imprime("El precio del euro en pesetas es: %lf\n", EURO);
    PON_ENTERO(2);
    PON_ENTERO(SUMA2(5));
    PON_ENTERO(SUMA(5,8));
    ESCRIBE ("%i\n", 2);
    return EXIT_SUCCESS;
}
