#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
    int *e;
    e = (int *) malloc (sizeof (int));

    *e = 10;

    printf ("&e: %p\n", &e);
    printf (" e: %p\n", e);
    printf ("*e: %i\n", *e);
    printf ("\n");


    free (e);


    return EXIT_SUCCESS;
}
