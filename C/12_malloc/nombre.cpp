#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 100

void
preguntar (char nom[N])
{
	printf ("Dime un Nombre: ");
	fgets (nom, N, stdin); // fgets para que pille el espacio
}

int
sacar_longitud (char nom[N])
{
	return strlen(nom);
}


int
main (int argc, char *argv[])
{
	char nom[N];
	int lon;
	char *mall;

	preguntar (nom);

	lon = sacar_longitud(nom);

	mall = (char *) malloc (lon);

	for (int i = 0; i < lon; i++)
		mall[i]=nom[i];

	printf ("\n");

	for (int i = 0; i < lon; i++)
		printf ("%c", mall[i]);

	printf ("\n");

	free (mall);



	return EXIT_SUCCESS;
}
