#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

int
main (int argc, char *argv[])
{
    char **lista;
    int rep;

    printf ("\nNumero de repeticiones: ");
    scanf (" %i", &rep);
    lista = (char **) malloc ( rep * sizeof (char *) );
    bzero (( void * ) lista, sizeof (lista));

    for (int i = 0; i < rep; i++)
    {
        printf ("Palabra numero %i: ", i+1);
        scanf (" %ms", &lista[i]);
    }

    printf("\n");

    for (int i = 0; i < rep; i++)
        printf ("La palabra numero %i es: %s\n", i+1, lista[i]);

    for (int i = 0; i < rep; i++)
        free (lista[i]);

    free(lista);
    printf("\n");

    return EXIT_SUCCESS;
}
