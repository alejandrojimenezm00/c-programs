#include <stdio.h>
#include <stdlib.h>

#define FILA1 2
#define FILA2 3
#define COLUMNA1 3
#define COLUMNA2 4

void
preguntar (int matriz1[FILA1][COLUMNA1], int matriz2[FILA2][COLUMNA2])
{
    for (int i = 0; i < FILA1; i++)
        for (int x = 0; x < COLUMNA1; x++)
        {
            printf ("Dime el valor para la posicion %i_%i de la primera matriz: ", i, x);
            scanf (" %i", &matriz1[i][x]);
        }

    for (int i = 0; i < FILA2; i++)
        for (int x = 0; x < COLUMNA2; x++)
        {
            printf ("Dime el valor para la posicion %i_%i de la segunda matriz: ", i, x);
            scanf (" %i", &matriz2[i][x]);

        }
}


void
rellenar (int matriz_resul[FILA1][COLUMNA2])
{
    for (int i = 0; i < FILA1; i++)
        for (int x = 0; x < COLUMNA2; x++)
            matriz_resul[i][x] = 0;
}


void
operar (int matriz1[FILA1][COLUMNA1], int matriz2[FILA2][COLUMNA2], int matriz_resul[FILA1][COLUMNA2])
{
    for (int i = 0; i < FILA1; i++)
        for (int x = 0; x < COLUMNA2; x++)
            for (int y = 0; y < FILA2; y++)
                matriz_resul[i][x] += matriz1[i][y] * matriz2[y][x];
}


void
imprimir (int matriz_resul[FILA1][COLUMNA2])
{
    system ("clear");
    printf ("\nEl resultado de la multiplicacion de las matrices es: \n");
    for (int i = 0; i < FILA1; i++)
    {
        for (int x = 0; x < COLUMNA2; x++)
            printf (" _%i_ ", matriz_resul[i][x]);
        printf("\n");
    }
    printf ("\n");
}


int
main (int argc, char *argv[])
{
    int matriz1[FILA1][COLUMNA1];
    int matriz2[FILA2][COLUMNA2];
    int matriz_resul[FILA1][COLUMNA2];

    preguntar (matriz1, matriz2);

    rellenar (matriz_resul);

    operar (matriz1, matriz2, matriz_resul);

    imprimir (matriz_resul);

    return EXIT_SUCCESS;
}
