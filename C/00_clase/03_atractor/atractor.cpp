#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main () {

	double numero;
	int ultima_raiz = 0;


	printf ("dime un numero para demostrar el atractor: ");
	scanf ("%lf", &numero);


	do{
		numero = sqrt(numero);
		printf("%lf\n", numero);
		ultima_raiz++;
	}while(numero > 1);

	printf ("se han hecho un total de %i\n", ultima_raiz);


	return EXIT_SUCCESS;
}
