#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdio_ext.h>
#include <time.h>
#include <ctype.h>

#define MAX 0x100
#define PALABRAS 101921 //numero total de palabras en el diccionario

int comprobar(int lon, char palabra[MAX], char letra) // con esta funcion compruebo si la letra que ha introducido el usuario existe en la
{                                                     // como tal en la palabra que se ha seleccionado. Si existe devuelve 0, sino 1

    for (int i = 0; i < lon; i++)
        if (palabra[i] == letra)
        {
            return 0;
        }
    return 1;
}

void imprimir(int lon, char palabra_jugador[MAX]) //Con esta funcion se imprime el programa
{
    for (int i = 0; i < lon; i++)
    {
        if (palabra_jugador[i])
            printf("%c", palabra_jugador[i]);
        else
            printf(" _ ");
    }
    printf("\n\n");
}

void registrar(int lon, char palabra[MAX], char palabra_jugador[MAX], char letra) // con esta funcion se registra la palabra del usuario en caso de que
{                                                                                 // no haya sido registrada antes

    for (int i = 0; palabra[i] != '\0'; i++)
        if (palabra[i] == letra)
            palabra_jugador[i] = letra;
}

int juega(char letra, int lon, char palabra[MAX], char palabra_jugador[MAX], char letra_fallada[8], unsigned fallos) //funcion para el juego
{
    //declaracion de una variable longitud para leer mejor el codigo

    lon = strlen(palabra) / sizeof(char);

    if (fallos == 8) //condicion de parada
        return 1;

    printf("letra: ");
    scanf(" %c", &letra);
    printf("\n%c\n", letra);
    printf("\n");

    if (comprobar(lon, palabra, letra) == 0) //llamadas a las funciones comprobar, en caso de que sea verdadero llamar a registrar
        registrar(lon, palabra, palabra_jugador, letra);
    else
        letra_fallada[fallos++] = letra; //array para almacenar las letras fallidas

    printf("Errores: %u\n", fallos);
    imprimir(lon, palabra_jugador); //llamada a imprimir

    if (strcasecmp(palabra_jugador, palabra) < 0)
        juega(letra, lon, palabra, palabra_jugador, letra_fallada, fallos); //llamada recursvida para repetir la funcion
    else
        return 0;
}

void seleccion(FILE *fichero, int palabra_seleccionada, char palabra[MAX], char **palabra_jugador)
{
    if (!(fichero = fopen("/usr/share/dict/british-english", "r")))
    {
        printf("Error al abrir el fichero\n");
        return;
    }
    for (int i = 0; i < palabra_seleccionada; i++)
        fscanf(fichero, "%s", palabra);
    fclose(fichero);

    // malloc para la asignacion de memoria en funcion del tamaño de la palabra, asi independientemente de la palabra que sea
    // siempre se asignará memoria. bzero para poner todo el contenido de palabra_jugador y asi evitar basura de memoria
    // __fpurge para limpiar el canal de entrada (al realizar el programa se intorducian valores sin quererlo, lo cual ha dado bastantes problemas)
    *palabra_jugador = (char *)malloc(strlen(palabra) / sizeof(char));
    bzero(*palabra_jugador, strlen(palabra) / sizeof(char));
}

void repetir(unsigned ganado, unsigned opcion, char palabra[MAX], char palabra_jugador[MAX], FILE *fichero, int palabra_seleccionada, char letra, int lon, unsigned fallos, char letra_fallada[8])
{

    printf("Has fallado estas letras: ");
    for (int i = 0; i < 8; i++)
        printf(" %c ", letra_fallada[i]);
    printf("\nLa palabra oculta era: ");
    for (int i = 0; i < lon; i++)
        printf("%c", palabra[i]);
    printf("\n");

    // switch case que permitirá elegir entre seguir jugando o salir
    switch (ganado)
    {
    case 0:
        printf("\tFelicidades, has ganado\n"
               "\tQue quieres hacer?\n"
               "\t1- Jugar otra vez.\n"
               "\t2- Exit.\n"
               "\tOpcion: ");
        scanf(" %u", &opcion);
        if (opcion == 1)
        {
            bzero(palabra, MAX);
            free(palabra_jugador);
            palabra_seleccionada = rand() % PALABRAS;
            seleccion(fichero, palabra_seleccionada, palabra, &palabra_jugador);
            __fpurge(stdin);
            ganado = juega(letra, lon, palabra, palabra_jugador, letra_fallada, fallos);
            repetir(ganado, opcion, palabra, palabra_jugador, fichero, palabra_seleccionada, letra, lon, fallos, letra_fallada);
        }
        break;

    default:
        printf("\tMala suerte, has perdido\n"
               "\tQue quieres hacer?\n"
               "\t1- Jugar otra vez.\n"
               "\t2- Exit.\n"
               "\tOpcion: ");
        scanf(" %u", &opcion);
        if (opcion == 1)
        {
            bzero(palabra, MAX);
            free(palabra_jugador);
            palabra_seleccionada = rand() % PALABRAS;
            seleccion(fichero, palabra_seleccionada, palabra, &palabra_jugador);
            __fpurge(stdin);
            ganado = juega(letra, lon, palabra, palabra_jugador, letra_fallada, fallos);
            repetir(ganado, opcion, palabra, palabra_jugador, fichero, palabra_seleccionada, letra, lon, fallos, letra_fallada);
        }
        break;
    }
}

int main(int argc, char *argv[])
{

    FILE *fichero;         //fichero a trabajar
    char palabra[MAX];     // array para almacenar la palabra elegida
    char letra_fallada[8]; // array para las letras fallidas
    char letra;            // variable para trabajar con las letras
    char *palabra_jugador; // variable para usar con malloc y reservar dinamicamente la memoria. usada para comprar la palabra almacenada y con la que trabajara el usuario
    int lon;
    unsigned ganado;
    unsigned opcion;
    unsigned fallos = 0;

    //codigo para seleccionar una palabra al azar
    srand(time(NULL));
    int palabra_seleccionada = rand() % PALABRAS;

    // funcion para asignar la palabra
    seleccion(fichero, palabra_seleccionada, palabra, &palabra_jugador);

    lon = strlen(palabra) / sizeof(char);

    // llamada a la funcion juega, interesa guardar el valor de retorno para operar con el en el switch case de mas adelante
    __fpurge(stdin);
    ganado = juega(letra, lon, palabra, palabra_jugador, letra_fallada, fallos); //si ganado recibe un 0 significa que el usuario a ganado, en caso
                                                                                 // de que recciba un 1 significa que ha perdido

    // Codigo que le mostrará al usuario las letras que ha fallado y la palabra seleccionada

    repetir(ganado, opcion, palabra, palabra_jugador, fichero, palabra_seleccionada, letra, lon, fallos, letra_fallada);

    printf("Nos vemos!!\n");

    free(palabra_jugador);

    return EXIT_SUCCESS;
}