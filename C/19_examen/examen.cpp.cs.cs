#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdio_ext.h>
#include <time.h>
#include <ctype.h>

#define MAX 0x100
#define PALABRAS 101921

int comprobar(int lon, char palabra[MAX], char letra)
{

    for (int i = 0; i < lon; i++)
        if (palabra[i] == letra)
            return 0;
    return 1;
}

void imprimir(int lon, char palabra_jugador[MAX])
{
    for (int i = 0; i < lon; i++)
    {
        if (palabra_jugador[i])
            printf("%c", palabra_jugador[i]);
        else
            printf(" _ ");
    }
    printf("\n\n");
}

void registrar(int lon, char palabra[MAX], char palabra_jugador[MAX], char letra)
{

    for (int i = 0; palabra[i] != '\0'; i++)
        if (palabra[i] == letra)
            palabra_jugador[i] = letra;
}

int juega(char letra, int lon, char palabra[MAX], char palabra_jugador[MAX], char letra_fallada[8], unsigned fallos)
{
    if (fallos == 8)
        return 1;

    printf("letra: ");
    scanf(" %c", &letra);
    printf("\n%c\n", letra);
    printf("\n");

    if (comprobar(lon, palabra, letra) == 0)
        registrar(lon, palabra, palabra_jugador, letra);
    else
        letra_fallada[fallos++] = letra;

    printf("Errores: %u\n", fallos);
    imprimir(lon, palabra_jugador);

    if (strcasecmp(palabra_jugador, palabra) != 0)
        juega(letra, lon, palabra, palabra_jugador, letra_fallada, fallos);
    else
        return 0;
}

void seleccion(FILE *fichero, int palabra_seleccionada, char palabra[MAX])
{
    if (!(fichero = fopen("/usr/share/dict/british-english", "r")))
    {
        printf("Error al abrir el fichero\n");
        return;
    }
    for (int i = 0; i < palabra_seleccionada; i++)
        fscanf(fichero, "%s", palabra);
    fclose(fichero);
}

int main(int argc, char *argv[])
{

    FILE *fichero;
    char palabra[MAX];
    char letra_fallada[8];
    char letra;
    char *palabra_jugador;
    int lon;
    unsigned opcion;
    bool ganado = false;
    unsigned fallos = 0;

    srand(time(NULL));
    int palabra_seleccionada = rand() % PALABRAS;

    seleccion(fichero, palabra_seleccionada, palabra);

    lon = strlen(palabra) / sizeof(char);

    palabra_jugador = (char *)malloc(lon);
    bzero(palabra_jugador, lon);
    __fpurge(stdin);

    ganado = juega(letra, lon, palabra, palabra_jugador, letra_fallada, fallos);

    printf("Has fallado estas letras: ");
    for (int i = 0; letra_fallada[i]; i++)
        printf(" %c ", letra_fallada[i]);
    printf("\nLa palabra oculta era: ");
    for (int i = 0; i < lon; i++)
        printf("%c", palabra[i]);
    printf("\n");

    free(palabra_jugador);

    switch (ganado)
    {
    case 0:
        printf("\tFelicidades, has ganado\n"
               "\tQue quieres hacer?\n"
               "\t1- Jugar otra vez.\n"
               "\t2- Exit.\n"
               "\tOpcion: ");
        scanf(" %u", &opcion);
        if (opcion == 1)
        {
            seleccion(fichero, palabra_seleccionada, palabra);
            juega(letra, lon, palabra, palabra_jugador, letra_fallada, fallos);
        }
        break;

    default:
        printf("\tMala suerte, has perdido\n"
               "\tQue quieres hacer?\n"
               "\t1- Jugar otra vez.\n"
               "\t2- Exit.\n"
               "\tOpcion: ");
        scanf(" %u", &opcion);
        if (opcion == 1)
        {
            seleccion(fichero, palabra_seleccionada, palabra);
            juega(letra, lon, palabra, palabra_jugador, letra_fallada, fallos);
        }
        break;
    }

    return EXIT_SUCCESS;
}