#include <stdio.h>
#include <stdlib.h>

#define F 10
#define C 10

#define info "\nvamos a jugar al hundir la flota\n\nEl tablero es de 10 x 10, el que,\nhunda todos los barcos del rival gana\n\n\n"


void
imprimir (char t[F][C])
{
    for (int i = 0; i < F; i++){
        for (int x = 0; x < C; x++)
            printf (" %c", t[i][x]);
        printf ("\n");
    };


}


void
coordenadas (unsigned *fila, unsigned *columna) {
        printf ("posicion de x del barco ");
        scanf (" %u", fila);
        printf ("posicion de y del barco ");
        scanf (" %u", columna);

}


int
main (int argc, char *argv[])
{
    printf (info);

    int punt_jug1 = 0;
    int punt_jug2 = 0;

    int fin_partida = 0;
    unsigned jugador = 1;
    unsigned turno = 0;
    int total_barcos = 21;

    unsigned fila;
    unsigned columna;

    char tablero_respuesta [F][C] = {
        { 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'},
        { '1', '1', '1', 'o', 'o', '1', 'o', 'o', 'o', '1'},
        { 'o', 'o', 'o', 'o', 'o', '1', 'o', 'o', 'o', '1'},
        { 'o', 'o', 'o', '1', 'o', 'o', 'o', 'o', 'o', 'o'},
        { 'o', 'o', 'o', 'o', 'o', '1', '1', '1', 'o', 'o'},
        { 'o', '1', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'},
        { 'o', '1', 'o', 'o', 'o', 'o', 'o', 'o', 'o', '1'},
        { 'o', '1', 'o', 'o', 'o', 'o', 'o', '1', 'o', '1'},
        { 'o', '1', 'o', 'o', '1', 'o', 'o', '1', 'o', '1'},
        { 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'}
    };

    char tablero_inicial [F][C] = {
        { 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'},
        { 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'},
        { 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'},
        { 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'},
        { 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'},
        { 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'},
        { 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'},
        { 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'},
        { 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'},
        { 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'}
    };



    do{
	coordenadas (&fila, &columna);
        if (tablero_respuesta [fila][columna] == 'o'){
            printf ("agua\n");
            if (jugador == 1)
                jugador = 2;
            else
                jugador = 1;
            turno++;
        };
        if (tablero_respuesta [fila][columna] == '1'){
            total_barcos--;
            printf ("dado\n quedan %i barcos\n", total_barcos);
            tablero_inicial [fila][columna] = 'x';
            if (jugador == 1){
                jugador = 2;
                punt_jug1++;
            }
            else{
                jugador = 1;
                punt_jug2++;
            }
            turno ++;

        };

        if (total_barcos == 0){
            fin_partida = 1;
        }

        imprimir (tablero_inicial);
        printf("\n");

    } while (fin_partida != 1);

    if (punt_jug1 < punt_jug2)
        printf ("\nHa ganado el jugador 2 con %i puntos\n", punt_jug2);
    else
        printf ("\nHa ganado el jugador 1 con %i puntos\n", punt_jug1);





    return EXIT_SUCCESS;
}


