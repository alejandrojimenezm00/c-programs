#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int
entrada (int rep, int *lista)
{
    return lista[rep - 1] < 0 ? 0 : 1;
}

int
main (int argc, char *argv[])
{
    int *lista;
    int rep = 0;

    lista = ( int * ) malloc (sizeof (int));

    do
    {
        printf ("Dime un numero: ");
        scanf  (" %i", &lista[rep++]);
        lista = ( int * ) realloc ( lista, (rep + 1) * sizeof (int) );

    } while (entrada (rep, lista));
    rep--;

    printf ("\n");
    for (int i = 0; i < rep; i++)
        printf ("numero %i = %i\n", i+1, lista[i]);
    printf ("\n");

    return EXIT_SUCCESS;
}
