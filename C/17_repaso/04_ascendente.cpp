#include <stdio.h>
#include <stdlib.h>


int
decreciente (int limit, int start = 1)
{
	printf ("%i ", start);
	return start == limit ? limit : decreciente (limit, start + 1);
}


int
main (int argc, char *argv[])
{

	int start = 1;
	int limit;

	printf ("Numero: ");
	scanf ("%i", &limit);
	printf ("\n");

	decreciente(limit, start);
	printf ("\n");

    return EXIT_SUCCESS;
}
