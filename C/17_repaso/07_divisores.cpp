#include <stdio.h>
#include <stdlib.h>


void
divisores (int num, int cont)
{
	if (cont == 0)
		return;
	if (num % cont == 0)
		printf ("El numero %i es divisor \n", cont);
	divisores (num, cont - 1);
}


int
main (int argc, char *argv[])
{
	int num;

	printf ("Dime un numero: ");
	scanf (" %i", &num);

	int cont = num;

	divisores (num, cont);

    return EXIT_SUCCESS;
}
