#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
    int A[] = {7, 3, 5, 8, 1, 2, 4};
    int n_elem = sizeof (A) / sizeof (int);
    int pivot = (A[0] + A[n_elem-1] + A[1]) / 3;

    for (int i=0, j=n_elem-1; i<=j; ){
        while (A[i]<pivot && i<j)
            i++;
        while (A[j]>pivot && i<j)
            j--;
        if (i<j) {
            int aux = A[i];
            A[i] = A[j];
            A[j] = aux;
        }
    }

    for (int i=0; i<n_elem; i++)
        printf ("%i ", A[i]);
    printf ("\n");

    return EXIT_SUCCESS;
}

