#include <stdio.h>
#include <stdlib.h>

int
contador ()
{
    static int n = 0;

    return ++n;

}

int
main (int argc, char *argv[])
{
    for (int i = 0; i < 10; i++)
        printf ("Tu ticket es: %i\n", contador() );

    return EXIT_SUCCESS;
}
