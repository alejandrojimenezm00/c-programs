#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct prueba{

	int a;
	double b;
	char c[20];
	char *d;
};

void
imprime (const struct prueba *prueba j)
{
	printf ("a: %i\n", (*j).a);
	printf ("b: %.2lf", j->b);  //el poner la flecha o el puntero dará el mismo resultado
	printf ("c: %s\n", j->c);
	printf ("d: %s\n", j->d);
}


int
main (int argc, char *argv[])
{
	struct prueba coso = {2, 2.5, "Txema", NULL};

	coso.d = (char *) malloc (20);
	strcpy (coso.d, "prueba");

	imprime (&coso);

    return EXIT_SUCCESS;
}
