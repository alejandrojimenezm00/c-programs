#include <stdio.h>
#include <stdlib.h>

int
imprimir (int num1, int num2) {

    return num1 + num2;
}

int
main (int argc, char *argv[])
{

    int (*pf) (int, int) = {imprimir};

    printf ("%i\n", pf(2, 3));

    return EXIT_SUCCESS;
}
