#include <stdio.h>
#include <stdlib.h>


struct empleados {

	char nombre[50];
	double sueldo;
	char telefono[20];
};


int
main (int argc, char *argv[])
{
	struct empleados Alex = {"Alejandro", 1500, "655941347"};

	printf ("El nombre del empleado es: %s, su sueldo es: %.2lf, su telefono es: %s\n", Alex.nombre, Alex.sueldo, Alex.telefono);

    return EXIT_SUCCESS;
}
