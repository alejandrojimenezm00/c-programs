#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

int
main (int argc, char *argv[])
{
	char **coche = NULL;
	char **p;
	int veces = 0;
	int entrada = 0;

	do
	{
		system ("clear");
		coche = (char **) realloc (coche, (entrada + 1) * sizeof (char *));
		printf ("Coche: ");
		veces = scanf ("%m[a-z A-Z]", coche + entrada++);
		__fpurge (stdin);
	} while (veces);

	system ("clear");
	p = coche;
	while (*p!=NULL)
	{
		printf ("%s\n", *p);
		p++;
	}


	for (char **i=coche; *i!=NULL; i++)
		free (*i);
	free (coche);

    return EXIT_SUCCESS;
}
