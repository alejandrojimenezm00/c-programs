#include <stdio.h>
#include <stdlib.h>

#define N 6

void
mostrar (int *ordenado[N])
{
	for (int i = 0; i<N; i++)
		printf ("%i", *ordenado[i]);
	printf ("\n");
}	

int
main (int argc, char *argv[])
{
	int sinorden[N] {1,5,4,7,2,9};
	int *ordenado[N];

	for (int i = 0; i<N; i++)
		ordenado[i] = &sinorden[i];

	mostrar (ordenado);

	for (int i = 0; i<N; i++)
		for (int x = 0; x<N-1; x++)
			if (*ordenado[i] < *ordenado[i+1])
			{
				int *aux = ordenado[x+1];
				ordenado[x+1] = ordenado[x];
				ordenado[x] = aux;
			}
	mostrar (ordenado);

    return EXIT_SUCCESS;
}
