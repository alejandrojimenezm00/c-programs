#include <stdio.h>
#include <stdlib.h>


void
repeticion (int num, char caracter)
{
	if (num == 0)
		return;
	printf ("%c", caracter);
	repeticion (num - 1, caracter);
}

void
letra (int num, char caracter, int cont)
{
	if (cont > num)
		return;
	repeticion (num + 1, caracter);
	letra (num, caracter + 1, cont + 1);
}


int
main (int argc, char *argv[])
{
	int num;
	char caracter = 'a';
	int cont = 0;

	printf ("Dime un numero: ");
	scanf (" %i", &num);

	letra (num - 1, caracter, cont);

	printf ("\n");

    return EXIT_SUCCESS;
}
