#include <stdio.h>
#include <stdlib.h>

#define N 10

int
suma (int resul = N)
{
	if (resul == 1)
		return 1;

	return resul + suma(resul-1); 

}



int
main (int argc, char *argv[])
{
	int resul = N;

	printf ("El resultado de la suma es: %i\n", suma(resul));

    return EXIT_SUCCESS;
}
