#include <stdio.h>
#include <stdlib.h>


int
suma (int num1, int num2)
{
	return num1 + num2;
}

int
resta (int num1, int num2)
{
	return num1 - num2;
}


int
main (int argc, char *argv[])
{
	int (*pf[])(int, int) = {suma, resta};

	int opcion, num1, num2;

	printf ("\n1. Suma \n"
		"2. Resta\n");
	scanf (" %i", &opcion);

	printf ("\nNumero 1: ");
	scanf (" %i", &num1);
	printf ("\nNumero 2: ");
	scanf (" %i", &num2);

	printf ("\nEl resultado es: %i\n", (pf[opcion-1](num1, num2)));


    return EXIT_SUCCESS;
}
