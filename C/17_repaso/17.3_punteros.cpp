#include <stdio.h>
#include <stdlib.h>

void
imprimir (int num1, int num2) {

    printf ("%i\n", num1 + num2);
}

int
main (int argc, char *argv[])
{

    void (*pf) (int, int) = {imprimir};

    pf(2,3);


    return EXIT_SUCCESS;
}
