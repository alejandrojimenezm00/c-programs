#include <stdio.h>
#include <stdlib.h>


void
binario (int num)
{
    if (num > 0)
        binario (num / 2);
    printf ("%i", num % 2);   
}


int
main (int argc, char *argv[])
{
    int num;

    printf ("Número: ");
    scanf (" %i", &num);

    printf ("\n");

    binario (num);

    printf ("\n");

    return EXIT_SUCCESS;
}
