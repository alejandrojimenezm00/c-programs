#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 0x100

int
main (int argc, char *argv[])
{
    char p[MAX];
    FILE *f = NULL;

    if (! (f = fopen("aprobado.txt", "w"))) {
        printf ("No se ha podido abrir el fichero");
        return EXIT_FAILURE;
    }

    do
    {
        scanf(" %s", p);
        fprintf (f, "%s", p);
    } while (strcmp(p, "fin") != 0);

    printf ("\n");

     fclose (f);

    return EXIT_SUCCESS;
}
