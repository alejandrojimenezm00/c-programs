#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 20

void
imprimir (char cadena[N], int longitud, unsigned contador)
{
	if (contador < longitud)
	{
		printf ("%c", cadena[contador]);
		imprimir (cadena, longitud, contador + 1);
	}
}


int
main (int argc, char *argv[])
{
	char cadena[N];
	unsigned contador = 0;

	printf ("Escribe algo: ");
	fgets (cadena, N, stdin);

	int longitud = strlen(cadena);

	imprimir (cadena, longitud, contador);
	printf ("\n");

    return EXIT_SUCCESS;
}
