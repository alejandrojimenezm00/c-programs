#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
	int num[] = {1, 5, 3, 4, 6, 9};
	int num_total = sizeof(num) / sizeof (int);
	int pivot = (num[0] + num[num_total - 1] + num[1]) / 3;

	system ("clear");

	for (int i = 0, j = num_total - 1; i <= j; )
	{
		while (num[i]<pivot && i<j)
			i++;
		while (num[i]>pivot && i<j)
			j--;
		if (i < j)
		{
			int aux = num[i];
			num[i] = num[j];
			num[j] = aux;
		}
	}

	printf ("Array Ordenado\n");
	for (int i = 0; i < num_total; i++)
		printf ("[%i]", num[i]);
	printf ("\n");

    return EXIT_SUCCESS;
}
