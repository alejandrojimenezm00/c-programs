#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct prueba
{
    char *d;
};

void
imprime (const struct prueba *j)
{
    printf ("Palabra: %s\n", j -> d);
}

int
main (int argc, char *argv[])
{

    struct prueba palabra = {NULL};

    palabra.d = (char *) malloc (sizeof (char));
    strcpy (palabra.d, "Alejandro");

    imprime (&palabra);

    return EXIT_SUCCESS;
}
