#include <stdio.h>
#include <stdlib.h>

#define N 10

int 
factorial (int num = N)
{
	return num == 1 ? 1 : num * factorial (num - 1);
}


int
main (int argc, char *argv[])
{
	int num = N;

	printf ("El factorial es: %i\n", factorial(num));

    return EXIT_SUCCESS;
}
