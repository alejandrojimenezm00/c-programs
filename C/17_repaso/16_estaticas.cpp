#include <stdio.h>
#include <stdlib.h>

#define N 10

int
ticket ()
{
	static int numero_ticket = 0;
	return ++numero_ticket;
}

int
main (int argc, char *argv[])
{
	for (int i = 0; i < N; i++)
		printf ("Ticket numero: %i\n", ticket());

    return EXIT_SUCCESS;
}
