#include <stdio.h>
#include <stdlib.h>

int main(){
    //ESTO ES CON FGETC
    FILE *f;
     char aux;
     char l[50];
    f=fopen("aprobado.txt","r");

    if(f==NULL){
        printf("no se pudo abrir");
        exit(1);
    }

    while(aux != EOF){
    aux = fgetc(f);
    printf("%c",aux);
}

    fclose(f);


    //ESTO ES CON FGETS


    f=fopen("aprobado.txt","r");

    if(f==NULL){
        printf("no se pudo abrir");
        exit(1);
    }

    while(!feof(f)){
        fgets(l,50,f);
        printf("%s",l);
    }


    fclose(f);


    //ESTO ES CON FSCANF

    f=fopen("aprobado.txt","r");

    if(f==NULL){
        printf("no se pudo abrir");
        exit(1);
    }


     while(!feof(f)){
        fscanf(f,"%c",&aux);
        printf("%c",aux);
    }

    fclose(f);

    return 0;
}
