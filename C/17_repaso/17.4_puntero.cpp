#include <stdio.h>
#include <stdlib.h>

int suma (int num1, int num2){
    return num1 + num2;
}

int resta (int num1, int num2){
    return num1 - num2;
}

int
main (int argc, char *argv[])
{

    int opcion, num1, num2;
    int (*pf[]) (int, int) = {suma, resta};

    printf ("1. Suma \n"
             "2. Resta \n"
             "Opcion: ");
    scanf (" %i", &opcion);

    printf ("Numero 1: ");
    scanf (" %i", &num1);
    printf ("\n");

    printf ("Numero 2: ");
    scanf (" %i", &num2);
    printf ("\n");

    printf ("El resultado de la operaciones: %i\n", (pf[opcion - 1](num1, num2)));

    return EXIT_SUCCESS;
}
