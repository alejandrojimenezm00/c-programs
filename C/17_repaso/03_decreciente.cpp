#include <stdio.h>
#include <stdlib.h>


int
decreciente (int start)
{
	printf ("%i ", start);
	return start == 1 ? 1 : decreciente (start-1);
}


int
main (int argc, char *argv[])
{

	int start;

	printf ("Numero: ");
	scanf ("%i", &start);
	printf ("\n");

	decreciente(start);
	printf ("\n");

    return EXIT_SUCCESS;
}
