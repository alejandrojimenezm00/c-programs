#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUM1 3
#define NUM2 3

int
main (int argc, char *argv[])
{
	int matriz[NUM1][NUM2];
        int posicion_inicial = 0;
	int suma = 0, multi = 1;

        srand (time(NULL));

	for (int i = 0; i < NUM1; i++)
		for (int x = 0; x < NUM1; x++)
			matriz[i][x] = rand () % 10 +1;

        system ("clear");

        for (int i = 0; i < NUM1; i++)
        {
            for (int x = 0; x < NUM1; x++)
                    multi *= matriz[i % NUM1][x % NUM1];

            suma += multi;
            multi = 1;
        }


	for (int i = 0; i < NUM1; i++)
	{
		for (int x = 0; x < NUM1; x++)
			multi *= matriz[i % NUM1][NUM1 - 1 - x];
		suma += multi;
		multi = 1;
	}



	for (int i = 0; i < NUM1; i++)
	{
		for (int x = 0; x < NUM1; x++)
			printf (" _%i_ ",matriz[i][x]);
		printf ("\n");

	}



        printf ("\nEl determinante es: %i\n", suma);


	return EXIT_SUCCESS;
}
