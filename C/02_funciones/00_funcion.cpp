#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>

int
suma (int op1, int op2){
    return op1 + op2;
}

int
suma2 (int *op1, int op2){
    return *op1 + op2;
}

int
resta (int op1, int op2){
    return op1 - op2;
}

int
resta2 (int *op1, int *op2){
    return *op1 - *op2;
}


int
main (int argc, char *argv[])
{
    int a = 10;
    int b = 20;
    int c, c2;

    c = suma (a, b);
    c = suma2 (&a, b);
    c2 = resta (a, b);
    c2 = resta2 (&a, &b);
    printf ("la suma es: %i\n", c);
    printf ("la suma con punteros es: %i\n", c);
    printf ("la resta es: %i\n", c2);
    printf ("la resta con punteros es: %i\n", c2);

    return EXIT_SUCCESS;
}

