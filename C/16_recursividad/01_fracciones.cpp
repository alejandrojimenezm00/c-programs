#include <stdio.h>
#include <stdlib.h>
#define MAX 20 

double
continua (double num, unsigned veces)
{
	if (veces == 0)
		return num;
	return num + 1 / continua (num, veces - 1);
}


int
main (int argc, char *argv[])
{
	double num;
	unsigned veces = MAX;

	printf ("Dime un numero para su fraccion continua: ");
	scanf (" %lf", &num);

	system ("clear");

	printf ("\n\n la fracción continua de %lf es: %lf\n\n", num, continua(num, veces));


    return EXIT_SUCCESS;
}
