#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 100

void
frase (char palabra[MAX], int longitud, int contador)
{

	if (contador < longitud){
		printf ("%c", palabra[contador]);
		frase (palabra, longitud, contador + 1);
	}
}


int
main (int argc, char *argv[])
{
	char palabra[MAX];
	int contador = 0;

	printf ("Dime una palabra: ");
	scanf (" %s", palabra);

	int longitud = strlen(palabra);

	system ("clear");

	frase (palabra, longitud, contador);

	printf ("\n");

	return EXIT_SUCCESS;
}
