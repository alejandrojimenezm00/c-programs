#include <stdio.h>
#include <stdlib.h>


unsigned
esprimo (unsigned num, unsigned veces)
{
	if (veces == 1)
		return 0;

	return num % veces == 0 ? 1 : esprimo (num, veces - 1);
}

int
main (int argc, char *argv[])
{
	unsigned num;

	printf ("Dime un numero para saber si es primo o no: ");
	scanf (" %u", &num);

	if (esprimo(num, num - 1) == 1)
		printf ("El numero %u no es primo\n", num);
	else
		printf ("El numero %u es primo\n", num);


    return EXIT_SUCCESS;
}
