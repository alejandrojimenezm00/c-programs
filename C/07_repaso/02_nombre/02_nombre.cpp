#include <stdio.h>
#include <stdlib.h>

#define M 20

void
preguntar (char nombre[M])
{
    printf ("dime tu nombre: ");
    scanf (" %s", nombre);
}


int
main (int argc, char *argv[])
{
    char nombre[M];

    preguntar (nombre);

    printf ("tu nombre es: %s\n", nombre);

    return EXIT_SUCCESS;
}
