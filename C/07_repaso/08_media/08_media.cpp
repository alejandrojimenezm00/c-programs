#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
    double cont;
    double suma = 0;

    for (int i = 0; i < 10; i++){
        printf ("dime un numero: \n");
        scanf ("%lf", &cont);

        suma += cont;
    }

    printf ("la media de los numeros es: %lf\n", suma/10);

    return EXIT_SUCCESS;
}
