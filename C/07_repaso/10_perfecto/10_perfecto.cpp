#include <stdio.h>
#include <stdlib.h>

void
preguntar (int *numero)
{
    printf ("dime numero para saber si es perfecto: ");
    scanf (" %i", numero);
}

void
operar (int division, int numero, int suma = 0)
{

    division = numero / 2;

    do {
        if (numero % division == 0)
            suma += division;
        division--;

    }while(division >= 1);

    if (numero == suma)
        printf ("el numero es perfecto \n");
    else
        printf ("el numero no es perfecto \n");

}

int
main (int argc, char *argv[])
{
    int num, suma = 0,  div;

    preguntar (&num);

    operar (div, num, suma);

    return EXIT_SUCCESS;
}
