#include <stdio.h>
#include <stdlib.h>


void
preguntar (unsigned *edad)
{
  printf ("Dime tu edad: ");
  scanf (" %u", edad);
}

int
main (int argc, char *argv[])
{
    unsigned edad;

    preguntar (&edad);

    printf ("tu edad es: %u\n", edad);

    return EXIT_SUCCESS;
}
