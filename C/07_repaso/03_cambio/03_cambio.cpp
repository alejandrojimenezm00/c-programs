#include <stdio.h>
#include <stdlib.h>

void
preguntar (unsigned *a, unsigned *b)
{
    printf ("vamos a intercambiar numeros\n");
    printf ("primer numero: ");
    scanf ("%u", a);
    printf ("segundo numero: ");
    scanf ("%u", b);
}

int
main (int argc, char *argv[])
{
    unsigned a, b, c;

    preguntar (&a, &b);

    c=a;
    a=b;
    b=c;

    printf ("con el cambio has conseguido que el primero numero sea: %u y el segundo es %u\n", a,b);

    return EXIT_SUCCESS;
}
