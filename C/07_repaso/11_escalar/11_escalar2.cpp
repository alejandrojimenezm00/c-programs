#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define M 10
#define N 10

void
rellenar (unsigned v0[N][M], unsigned v1[N])
{
    for (unsigned x = 0; x < N; x++)
        for (unsigned i = 0; i < M; i++)
            v0[i][x] = rand () % 10 +1; // con esto rellenamos las matrices con numeros del 1 al 10


    for (unsigned x = 0; x < N; x++)
        v1[x] = 1;
}

unsigned
operar (unsigned v0[N][M], unsigned v1[N])
{
    unsigned suma = 0;

    for (unsigned i = 0; i < N; i++)
        for (unsigned x = 0; x < M; x++)
            v1[i] *= v0[i][x];

    for (unsigned i = 0; i < N; i++)
        suma += v1[i];

    return suma;

}

int
main (int argc, char *argv[])
{

    unsigned v0[N][M], v1[N], suma;

    srand (time(NULL)); //seed random, con valor time (incluido en la libreria time.h) devuelve un valor aleatorio desde el segundo donde se ejecute el programa midiendo desde el año 1970

    rellenar (v0,  v1);

    suma = operar (v0, v1);


    system ("clear");
    printf ("\nLa suma de los 10 arrays es: %u \n\n", suma);


    return EXIT_SUCCESS;
}
