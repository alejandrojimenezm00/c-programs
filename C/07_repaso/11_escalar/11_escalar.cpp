#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 10

void
rellenar (unsigned v0[N], unsigned v1[N])
{
   for (unsigned i = 0; i < N; i++)
   {
       v0[i] = rand () % 10 +1;
       v1[i] = rand () % 10 +1; // con esto rellenamos las matrices con numeros del 1 al 10
   };

}

unsigned
operar (unsigned v0[N], unsigned v1[N])
{
    unsigned suma = 0;

    for (unsigned i = 0; i < N; i++)
         v0[i] *= v1[i];

     for (unsigned i = 0; i < N; i++)
         suma += v0[i];

     return suma;

}

int
main (int argc, char *argv[])
{

    unsigned v0[N], v1[N], suma;

    srand (time(NULL)); //seed random, con valor time (incluido en la libreria time.h) devuelve un valor aleatorio desde el segundo donde se ejecute el programa midiendo desde el año 1970

    rellenar (v0, v1);

    suma = operar (v0, v1);


    system ("clear");
    printf ("\nLa suma de los 2 arrays es: %u \n\n", suma);


    return EXIT_SUCCESS;
}
