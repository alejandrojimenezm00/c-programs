#include <stdio.h>
#include <stdlib.h>


void
preguntar (char *letra)
{
    printf ("dime una letra: ");
    scanf (" %c", letra);
}

int
main (int argc, char *argv[])
{
    char letra;

    preguntar (&letra);

    printf ("tu letra es: %c\n", letra);

    return EXIT_SUCCESS;
}
