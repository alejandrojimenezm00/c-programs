#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <vector>
using namespace std;


#define MAX 10


void
ordenar (int nums[])
{
    int indice = 0;

    int min = nums[0], max = nums[0];
    for (int i = 1 ; i < MAX; i++)
    {
        if (nums[i] < min)
            min = nums[i];
        if (nums[i] > max)
            max = nums[i];
    }

    int rango = max - min + 1;

    vector<int> numeros[rango];

    for (int i = 0; i < rango; i++)
        numeros[nums[i] - min].push_back(nums[i]);

    for (int i = 0; i < rango; i++)
    {
        vector<int>::iterator it;
        for (it = numeros[i].begin(); it != numeros[i].end(); ++it)
            nums[indice++] = *it;
    }
}



int
main (int argc, char *argv[])
{
    int nums [] = {1,5,2,8,4,3,3,6,8,9};
    //int nums[MAX];
    //srand (time(NULL));

    /*
       for (int i = 0; i < MAX; i++)
       nums[i] = rand () % 10 + 1;
       */

    system ("clear");
    printf ("Numeros sin ordenar: \n");
    for (int i = 0; i < MAX; i++)
        printf ("%i ", nums[i]);
    printf ("\n");



    ordenar (nums);
    printf ("Numeros ordenados: \n");
    for (int i = 0; i < MAX; i++)
        printf ("%i ", nums[i]);
    printf ("\n\n");



    return EXIT_SUCCESS;
}
