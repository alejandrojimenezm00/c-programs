#include <stdio.h>
#include <stdlib.h>
#include <math.h>


#define N 5 

int
main (int argc, char *argv[])
{
    int polinomio[N];
    int suma = 0;
    int multiplicador;

    printf ("Vamos a sumar un polinomio\n");
    for (int i = 0; i < N; i++)
    {
        printf ("Dime el valor %i del polinomio: ", i+1);
        scanf (" %i", &polinomio[i]);
    }

    printf ("Ahora dime el valor por el que lo quieres multiplicar: ");
    scanf (" %i", &multiplicador);

    for (int i = 1; i < N; i++)
    {
        suma += polinomio[i-1] *= pow(multiplicador, i); // suma de todo el array elevado al incremento. buscar la forma de realizar una potencia

    }


    system ("clear");
    printf ("\n\nla suma del polinomio es: %i\n\n", suma);

    return EXIT_SUCCESS;
}
