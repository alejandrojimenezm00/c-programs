#include <stdio.h>
#include <stdlib.h>



void
leer_vector (double *var1, double *var2, double *var3) {

    printf("Dime los 3 valores del vector (formato: (x, y, z) o [x, y, z]) \n");
    scanf ("%*1[[(]%lf%*1[,] %lf%*1[,] %lf%*1[])]", var1, var2, var3);

}


void
imprimir_vector (double x, double y, double z){

    printf ("El vector que has introducido es %.3lfx, %.3lfy, %.3lfz\n", x, y, z);
}


int
main (int argc, char *argv[])
{
	double x = 0,
	       y = 0,
	       z = 0;

        leer_vector (&x, &y, &z);
        imprimir_vector (x, y, z);




	return EXIT_SUCCESS;
}
